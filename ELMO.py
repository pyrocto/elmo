from typing import List, Set, Dict, Tuple, Optional
from dataclasses import dataclass
from enum import Enum

@dataclass(frozen=True)
class Prestate:
  observations: List[Observation]

  def __str__(self):
    return f'Ps[{", ".join(list(map(str, self.observations)))}]'

Label = Enum('Label', 'RECEIVE SEND')

@dataclass(frozen=True)
class Observation:
  label: Label
  message: Premessage
  witness: int

  def __str__(self):
    return f'Ob({self.label.name}({self.message}, {self.witness})'

@dataclass(frozen=True)
class Premessage:
  state: Prestate
  author: int

  def __str__(self):
    return f'Msg({self.state}, {self.author})'

# Check that every message received or sent in m has been received in the prefix by the component
def fullNode(m: Premessage, prefix: List[Observation], component: int) -> bool:

  p = m.state
  a = m.author

  for ob2 in p.observations:
    l2 = ob2.label
    m2 = ob2.message
    w2 = ob2.witness
    p2 = m2.state
    a2 = m2.author

    if l2 == Label.SEND:
      # a is sending, so a2 is a
      if Observation(Label.RECEIVE, m2, component) not in prefix:
        return False
      continue

    else: # l2 == Label.RECEIVE

      if a2 == component:
        # If a got a message from component, component must have sent it
        if Observation(Label.SEND, m2, component) not in prefix:
          return False
        continue

      else:
        # If a got a message from a2, component must also have received it from a2
        if Observation(Label.RECEIVE, m2, component) not in prefix:
          return False
        continue

  return True


def update(m: Premessage, component: int, weights: List[float], threshold: float, curState: List[Prestate], curEq: Set[int]) -> bool:
  p = m.state
  a = m.author
  lp = len(p.observations)
  la = len(curState[a].observations)
  if lp >= la and p.observations[0 : la] == curState[a].observations:
    # new info; update state
    curState[a] = Prestate([*p.observations, Observation(Label.SEND, m, a)])
    return True
  
  if (lp + 1 <= la and
     curState[a].observations[0 : lp] == p.observations and 
     curState[a].observations[lp] == Observation(Label.SEND, m, a)):
    # old info
    return True

  # a is equivocating
  newEq = { *curEq }
  newEq.add(a)

  if sum(list(map(lambda x: weights[x], newEq))) > threshold:
    return False

  curEq.add(a)
  return True

def isProtocol(prestate: Prestate, component: int, weights: List[float], threshold: float) -> bool:
  i = 0
  curState = list(map(lambda x: Prestate([]), weights))
  curEq: Set[int] = set()

  for ob in prestate.observations:
    l = ob.label
    m = ob.message
    p = m.state
    a = m.author
    w = ob.witness
    # Everything except the current observation
    prefix = prestate.observations[0 : i]
    i += 1

    # These observations are supposed to have been witnessed by component
    if w != component:
      return False

    # If the message came from the component
    if a == component:

      if l == Label.SEND:
        # SENDs should contain the entire current state; otherwise, they're not protocol
        if p.observations != prefix:
          return False

        continue

      else: # l == Label.RECEIVE
        # If component received a message from itself, it must have sent it
        if Observation(Label.SEND, m, component) not in prefix:
          return False

        continue

    else: # a != component

      # Component should not have SENDs from other authors in its state, only RECEIVEs 
      if l == Label.SEND: 
        return False

      else: # l == Label.RECEIVE
        
        # Component must have sent or received all the observations in the message already
        if not fullNode(m, prefix, component):
          return False

        if not update(m, component, weights, threshold, curState, curEq):
          return False

  return True


def valid(weights: List[float], threshold: float, label: Label, state: Prestate, message: Optional[Premessage]) -> bool:
  if label == Label.SEND:
    return message is None
  else: # label == Label.RECEIVE
    return message is not None and isProtocol(message.state, message.author, weights, threshold)

def tau(component: int, label: Label, state: Prestate, message: Optional[Premessage]) -> Tuple[Prestate, Optional[Premessage]]:
  if label == Label.SEND:
    if message is None:
      m = Premessage(state, component)
      ob = Observation(Label.SEND, m, component)
      s = Prestate([*state.observations, ob])
      return (s, m)
    else:
      return (state, None)

  else: # label == Label.RECEIVE
    if message is None:
      return (state, None)
    else:
      ob = Observation(Label.RECEIVE, message, component)
      s = Prestate([*state.observations, ob])
      return (s, None)



