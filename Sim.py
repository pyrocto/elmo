#! /opt/local/bin/python3

from typing import List, Set, Dict, Tuple, Optional
import random
import sys
from ELMO import tau, isProtocol, Prestate, Label, Observation, Premessage

def main(argv):
  debug = False
  if len(argv) > 0:
    debug = True

  cs = [(1, Label.SEND, 1), (0, Label.RECEIVE, 1)] # debug
  j = 0 # debug

  # Create three communicating ELMO validators and let them interact
  num: int = 3
  weights: List[float] = [random.random() for i in range(num)]
  threshold: float = random.random()
  if debug:
    threshold = 0
  states: List[Prestate] = [Prestate([]) for i in range(num)]

  print (weights, threshold)

  # List of messages sent by each component
  sent: List[List[Premessage]] = [[] for i in range(num)]
  # Whether anyone has sent a message yet
  oneSent = False

  done: str = ''
  while done == '':
    # Choose a random component
    c: int = random.randint(0, num - 1)
    if debug:
      c = cs[j][0]

    # Choose send or receive
    l: Label = Label(random.randint(1, 2))
    if debug:
      l = cs[j][1]

    print (c, l)
    t: Tuple[Prestate, Optional[Premessage]]
    if l == Label.SEND:
      # if send, add it to the sent set
      t = tau(c, l, states[c], None)
      states[c] = t[0]
      # t[1] is never None for a SEND
      sent[c].append(t[1])
      oneSent = True
    elif oneSent: # l == Label.RECEIVE and there's a message to receive
      # pick a random validator that has sent a message
      i = random.randint(0, num - 1)
      while len(sent[i]) == 0:
        i = random.randint(0, num - 1)

      if debug:
        i = cs[j][2]

      j: int = random.randint(0, len(sent[i]) - 1)
      m: Premessage = sent[i][j]
      # have c receive it
      t = tau(c, l, states[c], m)
      # update the state
      states[c] = t[0]
    else: # l == Label.RECEIVE but there's no message to receive
      continue

    if not isProtocol(states[c], c, weights, threshold):
      print("Not protocol:")
      print(f"component {c} {l} message {j} from component {i}")
      print(list(map(str, states)))
      break

    # print all the states
    print(list(map(str, states)))

    done = input('')
    j += 1 # debug


if __name__ == "__main__":
    main(sys.argv[1:])
